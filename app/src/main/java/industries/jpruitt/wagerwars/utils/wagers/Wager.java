package industries.jpruitt.wagerwars.utils.wagers;

import android.net.Uri;

import java.sql.Time;

/**
 * Created by jpruitt on 9/1/15.
 */
public class Wager {

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public Time getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(Time timeLeft) {
        this.timeLeft = timeLeft;
    }

    private double amount;
    private String description;
    private String homeTeam;
    private String awayTeam;
    private String category;
    private String creator;
    private String winner;
    private Time startTime;
    private Time endTime;
    private Time timeLeft;

    public Wager(Uri uri) {

        amount = Double.parseDouble(uri.getQueryParameter("wageramount"));
        description = uri.getQueryParameter("description");
        homeTeam = uri.getQueryParameter("hometeam");
        awayTeam = uri.getQueryParameter("awayteam");
        category = uri.getQueryParameter("category");
        creator = uri.getQueryParameter("creator");
    }
}
