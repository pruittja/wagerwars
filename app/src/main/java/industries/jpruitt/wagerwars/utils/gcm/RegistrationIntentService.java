package industries.jpruitt.wagerwars.utils.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import industries.jpruitt.wagerwars.R;

/**
 * Created by jpruitt on 8/16/15.
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String SERVER_URL = "http://wagerwars-jpruitt.rhcloud.com/register_device";
    private static String email = "";
    private boolean sentToken = false;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {

            if (intent.getStringExtra("email") != null)
                email = intent.getStringExtra("email");

            String token = InstanceID.getInstance(this).getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.d("TOKEN", token);
            sendRegistrationToServer(token, email);

            sharedPreferences.edit().putBoolean("TokenSent", sentToken).apply();

        } catch (IOException e) {

            sharedPreferences.edit().putBoolean("TokenSent", false).apply();

        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("registrationComplete"));

    }

    private void sendRegistrationToServer(String token, String email) {

        try {

            URLConnection connection = new URL(SERVER_URL).openConnection();
            connection.setDoOutput(true);

            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write("email=" + email + "&deviceID=" + token);

            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String decode = "";
            while ((decode = in.readLine()) != null)
                Log.d("sinful", decode);

            sentToken = true;

        } catch (Exception e) {
            Log.d("sinful", e.getMessage());
        }
    }
}
