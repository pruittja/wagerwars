package industries.jpruitt.wagerwars.utils.gcm;

import android.content.Intent;

/**
 * Created by jpruitt on 8/16/15.
 */
public class InstanceIdListenerService extends com.google.android.gms.iid.InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {

        startService(new Intent(this, RegistrationIntentService.class));

    }
}
