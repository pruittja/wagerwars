package industries.jpruitt.wagerwars.view.startup.register;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;
import industries.jpruitt.wagerwars.R;
import industries.jpruitt.wagerwars.view.startup.ActionProcessButton;
import industries.jpruitt.wagerwars.view.startup.login.LoginActivity;
import industries.jpruitt.wagerwars.view.startup.ProgressGenerator;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;


public class RegistrationActivity extends LoginActivity implements ProgressGenerator.OnCompleteListener {

    private static String REGISTRATION_SERVER_URL = "http://wagerwars-jpruitt.rhcloud.com/register";
    private static EditText emailText;
    private static EditText passwordText;
    private static EditText firstNameText;
    private static EditText lastNameText;
    private static ActionProcessButton submitButton;
    private RegistrationActivity activity;
    private String email;
    private String password;
    private String secondPassword;
    private String firstName;
    private String lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        emailText = (EditText)findViewById(R.id.emailText);
        passwordText = (EditText)findViewById(R.id.passwordText);
        firstNameText = (EditText)findViewById(R.id.firstNameText);
        lastNameText = (EditText)findViewById(R.id.lastNameText);
        submitButton = (ActionProcessButton)findViewById(R.id.btnRegister);

        activity = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onComplete() {

    }

    @Override
    protected void onResume() {

        super.onResume();

    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    public void register(View v) {

        email = emailText.getText().toString().toLowerCase().trim();
        password = passwordText.getText().toString().trim();
        secondPassword = ((EditText)findViewById(R.id.redundantText)).getText().toString().trim();
        firstName = firstNameText.getText().toString().toLowerCase().trim();
        lastName = lastNameText.getText().toString().toLowerCase().trim();

        isDoneLoggingIn = false;

        SweetAlertDialog sad = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Uh-Oh!")
                .setConfirmText("OK");

        sad.setCancelable(true);

        final ProgressGenerator progressGenerator = new ProgressGenerator(activity);

        if (!email.equals("")) {

            try {

                InternetAddress ia = new InternetAddress(email);
                ia.validate();

                if (password.length() > 7) {
                    char valid = isAcceptablePassword(password);

                    if (valid == '\0') {
                        if (secondPassword.equals(password)) {
                            if (!firstName.equals("")) {
                                if (firstName.matches("[a-zA-Z\\-_ ’'‘ÆÐƎƏƐƔĲŊŒẞÞǷȜæðǝəɛɣĳŋœĸſßþƿȝĄƁÇĐƊĘĦĮƘŁØƠŞȘŢȚŦŲƯY̨Ƴąɓçđɗęħįƙłøơşșţțŧųưy̨ƴÁÀÂÄǍĂĀÃÅǺĄÆǼǢƁĆĊĈČÇĎḌĐƊÐÉÈĖÊËĚĔĒĘẸƎƏƐĠĜǦĞĢƔáàâäǎăāãåǻąæǽǣɓćċĉčçďḍđɗðéèėêëěĕēęẹǝəɛġĝǧğģɣĤḤĦIÍÌİÎÏǏĬĪĨĮỊĲĴĶƘĹĻŁĽĿʼNŃN̈ŇÑŅŊÓÒÔÖǑŎŌÕŐỌØǾƠŒĥḥħıíìiîïǐĭīĩįịĳĵķƙĸĺļłľŀŉńn̈ňñņŋóòôöǒŏōõőọøǿơœŔŘŖŚŜŠŞȘṢẞŤŢṬŦÞÚÙÛÜǓŬŪŨŰŮŲỤƯẂẀŴẄǷÝỲŶŸȲỸƳŹŻŽẒŕřŗſśŝšşșṣßťţṭŧþúùûüǔŭūũűůųụưẃẁŵẅƿýỳŷÿȳỹƴźżžẓ]")) {
                                    if (!lastName.equals("")) {
                                        if (lastName.matches("[a-zA-Z\\-_ ’'‘ÆÐƎƏƐƔĲŊŒẞÞǷȜæðǝəɛɣĳŋœĸſßþƿȝĄƁÇĐƊĘĦĮƘŁØƠŞȘŢȚŦŲƯY̨Ƴąɓçđɗęħįƙłøơşșţțŧųưy̨ƴÁÀÂÄǍĂĀÃÅǺĄÆǼǢƁĆĊĈČÇĎḌĐƊÐÉÈĖÊËĚĔĒĘẸƎƏƐĠĜǦĞĢƔáàâäǎăāãåǻąæǽǣɓćċĉčçďḍđɗðéèėêëěĕēęẹǝəɛġĝǧğģɣĤḤĦIÍÌİÎÏǏĬĪĨĮỊĲĴĶƘĹĻŁĽĿʼNŃN̈ŇÑŅŊÓÒÔÖǑŎŌÕŐỌØǾƠŒĥḥħıíìiîïǐĭīĩįịĳĵķƙĸĺļłľŀŉńn̈ňñņŋóòôöǒŏōõőọøǿơœŔŘŖŚŜŠŞȘṢẞŤŢṬŦÞÚÙÛÜǓŬŪŨŰŮŲỤƯẂẀŴẄǷÝỲŶŸȲỸƳŹŻŽẒŕřŗſśŝšşșṣßťţṭŧþúùûüǔŭūũűůųụưẃẁŵẅƿýỳŷÿȳỹƴźżžẓ]")) {

                                            submitButton.setEnabled(false);
                                            emailText.setEnabled(false);
                                            passwordText.setEnabled(false);
                                            findViewById(R.id.redundantText).setEnabled(false);
                                            firstNameText.setEnabled(false);
                                            lastNameText.setEnabled(false);
                                            progressGenerator.start(submitButton, activity);

                                            new AsyncLogin().execute(
                                                    new String[]{email,
                                                            password,
                                                            firstName,
                                                            lastName
                                                    }
                                            );

                                        } else {

                                            sad.setContentText("Your last name contains illegal characters.").show();
                                        }

                                    } else {

                                        sad.setContentText("Please provide a last name.").show();
                                    }

                                } else {

                                    sad.setContentText("Your first name contains illegal characters.").show();
                                }

                            } else {

                                sad.setContentText("Please provide a first name.").show();
                            }

                        } else {

                            sad.setContentText("Passwords do not match.").show();
                        }


                    } else {

                        if (Character.isUpperCase(valid))
                            sad.setContentText("Passwords must contain at least one upper case letter.").show();

                        else if (Character.isLowerCase(valid))
                            sad.setContentText("Passwords must contain at least one lower case letter.").show();

                        else
                            sad.setContentText("Passwords must contain at least one digit.").show();
                    }

                } else {

                    sad.setContentText("Passwords must contain at least 8 characters.").show();
                }

            } catch (AddressException e) {

                sad.setContentText(email + " is not a valid email address.").show();
            }

        } else {

            sad.setContentText("Please provide an email address.").show();

        }

        isDoneLoggingIn = true;
    }

    private char isAcceptablePassword(String pass) {

        boolean hasLowerCase = false;
        boolean hasUpperCase = false;
        boolean hasDigit = false;

        for (int c : pass.toCharArray()) {
            if (Character.isDigit(c))
                hasDigit = true;
            else if (Character.isLetter(c) && Character.isLowerCase(c))
                hasLowerCase = true;

            else if (Character.isLetter(c) && Character.isUpperCase(c))
                hasUpperCase = true;
        }

        if (hasLowerCase) {
            if (hasUpperCase) {
                if (hasDigit) {

                    return '\0';

                } else {

                    return '1';
                }

            } else {

                return 'A';
            }

        } else {

            return 'a';
        }
    }

    class AsyncLogin extends AsyncTask<String, String, String> {

        String email;
        String password;

        @Override
        protected String doInBackground(String... credentials) {

            email = credentials[0];
            password = credentials[1];
            String firstName = credentials[2];
            String lastName = credentials[3];

            try {

                URLConnection connection = new URL(REGISTRATION_SERVER_URL).openConnection();
                connection.setDoOutput(true);

                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write("email=" + email + "&password=" + password + "&firstname=" + firstName + "&lastname=" + lastName);
                out.close();

                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String response = "";
                while ((response = in.readLine()) != null) {

                    if (response.equals("Account created successfully.")) {

                        return "Accepted";
                    }
                }

            } catch (Exception e) {

                SweetAlertDialog pDialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
                pDialog.setContentText("A network error occurred. Please ensure that you are connected to the internet.");
                pDialog.setTitleText("Uh-Oh!");
                pDialog.setCancelable(true);
                pDialog.show();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            isDoneLoggingIn = true;
//
            if (result != null && result.equals("Accepted")) {

                Intent intent = new Intent(activity, LoginActivity.class);
                intent.putExtra("email", email);
                intent.putExtra("password", password);
                startActivity(intent);

            } else {

                submitButton.setEnabled(true);
                emailText.setEnabled(true);
                passwordText.setEnabled(true);
                findViewById(R.id.redundantText).setEnabled(true);
                firstNameText.setEnabled(true);
                lastNameText.setEnabled(true);
                submitButton.setProgress(0);

                SweetAlertDialog pDialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
                pDialog.setContentText("Email is already in use.");
                pDialog.setTitleText("Uh-Oh!");
                pDialog.setCancelable(true);
                pDialog.show();

            }
        }
    }
}
