package industries.jpruitt.wagerwars.view.startup;

import android.os.Handler;

import java.util.Random;

import industries.jpruitt.wagerwars.view.startup.login.LoginActivity;

public class ProgressGenerator {

    public interface OnCompleteListener {

        void onComplete();
    }

    private OnCompleteListener mListener;
    private int mProgress;

    public ProgressGenerator(OnCompleteListener listener) {
        mListener = listener;
    }

    public void start(final ProcessButton button, final LoginActivity a) {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!a.isDoneLoggingIn) {
                    button.setProgress(10);
                    handler.postDelayed(this, generateDelay());

                } else {
                    button.setProgress(100);
                    mListener.onComplete();
                }
            }
        }, generateDelay());
    }

    private Random random = new Random();

    private int generateDelay() {
        return random.nextInt(1000);
    }
}
