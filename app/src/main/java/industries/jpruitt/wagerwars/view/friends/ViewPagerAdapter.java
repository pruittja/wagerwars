package industries.jpruitt.wagerwars.view.friends;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;

import industries.jpruitt.wagerwars.view.friends.tabs.FindFriendsFragment;
import industries.jpruitt.wagerwars.view.friends.tabs.FriendListFragment;
import industries.jpruitt.wagerwars.R;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    Context context;

    public ViewPagerAdapter(FragmentManager fm, Context c) {

        super(fm);
        context = c;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FriendListFragment();

            case 1:
                return new FindFriendsFragment();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        SpannableStringBuilder sb;
        Drawable d;

        if (position == 0) {

            d = ContextCompat.getDrawable(context, R.drawable.friends);
            sb = new SpannableStringBuilder("  Friend List");

        } else {

            d = ContextCompat.getDrawable(context, R.drawable.find_friends);
            sb = new SpannableStringBuilder("  Find Friends");

        }

        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
        sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return sb;
    }
}
