package industries.jpruitt.wagerwars.view.startup.login;

import android.app.Activity;
import android.content.Intent;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import industries.jpruitt.wagerwars.R;
import industries.jpruitt.wagerwars.utils.URLParser;
import industries.jpruitt.wagerwars.utils.gcm.RegistrationIntentService;
import industries.jpruitt.wagerwars.view.MainActivity;
import industries.jpruitt.wagerwars.view.startup.ActionProcessButton;
import industries.jpruitt.wagerwars.view.startup.ProgressGenerator;
import industries.jpruitt.wagerwars.view.startup.register.RegistrationActivity;

public class LoginActivity extends Activity implements ProgressGenerator.OnCompleteListener {

    private static final String LOGIN_SERVER_URL = "http://wagerwars-jpruitt.rhcloud.com/login";
    private static final String REMEMBER_LOGIN_PREFERENCE = "remember_login";
    private static final String USERNAME_PREFERENCE = "username";
    private static final String PASSWORD_PREFERENCE = "password";

    public boolean isDoneLoggingIn = false;
    private LoginActivity activity;
    private EditText passwordText;
    private EditText emailText;
    private ActionProcessButton submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ((TextView)findViewById(R.id.logoImage)).setTypeface(Typeface.createFromAsset(getAssets(),
                "fonts/painting_the_light.ttf"));

        passwordText = (EditText) findViewById(R.id.passwordText);
        emailText = (EditText)findViewById(R.id.emailText);
        submitButton = (ActionProcessButton) findViewById(R.id.btnSignIn);

        activity = this;

        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(REMEMBER_LOGIN_PREFERENCE, false)) {

            emailText.setText(PreferenceManager.getDefaultSharedPreferences(this).getString(USERNAME_PREFERENCE, ""));
            passwordText.setText(PreferenceManager.getDefaultSharedPreferences(this).getString(PASSWORD_PREFERENCE, ""));
            ((CheckBox)findViewById(R.id.checkBox)).setChecked(true);

        } else if (getIntent().getStringExtra("email") != null) {
            if (getIntent().getStringExtra("password") != null) {

                emailText.setText(getIntent().getStringExtra("email").toLowerCase().trim());
                passwordText.setText(getIntent().getStringExtra("password"));

                login(this.findViewById(android.R.id.content));

            } else {

                emailText.setText(getIntent().getStringExtra("email"));
            }
        }
    }

    @Override
    public void onComplete() {

    }

    @Override
    protected void onResume() {

        super.onResume();

        getIntent().putExtra("email", emailText.getText().toString());
        onCreate(null);
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showRegistration(View v) {
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    public void login(View v) {

        isDoneLoggingIn = false;

        final ProgressGenerator progressGenerator = new ProgressGenerator(activity);

        submitButton.setEnabled(false);
        emailText.setEnabled(false);
        passwordText.setEnabled(false);
        progressGenerator.start(submitButton, activity);

        new AsyncLogin().execute(new String[]{emailText.getText().toString(), passwordText.getText().toString()});

    }

    private boolean checkPlayServices() {

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, 9000).show();

            } else {

                finish();
            }

            return false;
        }

        return true;
    }

    class AsyncLogin extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... credentials) {

            String email = credentials[0];
            String password = credentials[1];

            try {

                URLConnection connection = new URL(LOGIN_SERVER_URL).openConnection();
                connection.setDoOutput(true);

                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write("email=" + email + "&password=" + password);
                out.close();

                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String response = "";
                while ((response = in.readLine()) != null) {

                    if (response.contains("correctlogin=true")) {

                        return response;
                    }
                }

            } catch (Exception e) {

            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            String email = ((EditText)findViewById(R.id.emailText)).getText().toString().toLowerCase().trim();

            if (result != null) {

                if (((CheckBox)findViewById(R.id.checkBox)).isChecked()) {

                    PreferenceManager.getDefaultSharedPreferences(activity)
                            .edit()
                            .putBoolean(REMEMBER_LOGIN_PREFERENCE, true)
                            .putString(USERNAME_PREFERENCE, email)
                            .putString(PASSWORD_PREFERENCE,
                                    ((EditText)findViewById(R.id.passwordText)).getText().toString().trim())
                            .commit();

                } else {

                    PreferenceManager.getDefaultSharedPreferences(activity)
                            .edit()
                            .putBoolean(REMEMBER_LOGIN_PREFERENCE, false)
                            .commit();
                }


                if (checkPlayServices()) {

                    isDoneLoggingIn = true;
                    Map<String, String> values = null;

                    try {

                        values = URLParser.splitQuerySingleValue(result);

                    } catch(UnsupportedEncodingException e) {

                    }

                    Intent intent = new Intent(activity, RegistrationIntentService.class);
                    intent.putExtra("email", email);
                    startService(intent);

                    submitButton.setEnabled(true);
                    emailText.setEnabled(true);
                    passwordText.setEnabled(true);

                    intent = new Intent(activity, MainActivity.class);
                    intent.putExtra("email", email);

                    if (values != null) {
                        intent.putExtra("firstname", values.get("firstname"));
                        intent.putExtra("lastname", values.get("lastname"));
                        intent.putExtra("balance", Double.parseDouble(values.get("balance")));
                    }

                    startActivity(intent);

                } else {

                    SweetAlertDialog pDialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
                    pDialog.setContentText("Google Play Services are not available at the moment.");
                    pDialog.setTitleText("Uh-Oh!");
                    pDialog.setCancelable(true);
                    pDialog.show();
                }

            } else {

                SweetAlertDialog sad = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
                sad.setCancelable(true);
                sad.setContentText("Invalid username or password.")
                    .setTitleText("Uh-Oh!")
                        .setConfirmText("Okay")
                        .show();

                getIntent().putExtra("email", email);
                onCreate(null);
            }

            isDoneLoggingIn = true;
            submitButton.setEnabled(true);
            emailText.setEnabled(true);
            passwordText.setEnabled(true);
        }
    }
}






