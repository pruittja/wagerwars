package industries.jpruitt.wagerwars.view.friends.tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import industries.jpruitt.wagerwars.R;

public class FriendListFragment extends Fragment {

    List<String> confirmedFriends;
    List<String> outgoingFriendRequests;
    List<String> incomingFriendRequests;

    String UPLOAD_PROFILE_PICTURE_URL = "http://wagerwars-jpruitt.rhcloud.com/users/profile/put/image";
    String DOWNLOAD_PROFILE_PICTURE_URL = "http://wagerwars-jpruitt.rhcloud.com/users/profile/get/image";

    public FriendListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        this.startActivityForResult(photoPickerIntent, 100);

        return inflater.inflate(R.layout.fragment_friend_list, container, false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                    Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case 100:
                if(resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContext().getContentResolver().query(
                            selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();

                    try {

                        /* POST image
                        URLConnection connection = new URL(UPLOAD_PROFILE_PICTURE_URL).openConnection();
                        connection.setRequestProperty("Content-Type", "application/json;");
                        connection.setDoOutput(true);

                        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                        out.write("{\"email\":\"" + getActivity().getIntent().getStringExtra("email") + "\",\"image\":\""
                                + Base64.encodeToString(IOUtils.toByteArray(new FileInputStream(filePath)), Base64.NO_WRAP)
                                + "\"}");
                        //IOUtils.copy(new FileInputStream(filePath), out);
                        out.close();

                        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                        String response = "";
                        while ((response = in.readLine()) != null) {

                        }
*/
                        URLConnection connection = new URL(DOWNLOAD_PROFILE_PICTURE_URL).openConnection();
                        connection.setDoOutput(true);

                        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                        out.write("email=" + getActivity().getIntent().getStringExtra("email"));
                        out.close();

                        getView().findViewById(R.id.imageView).setBackgroundDrawable(new BitmapDrawable(getContext().getResources(), BitmapFactory.decodeStream(connection.getInputStream())));

                    } catch (Exception e) {

                        Log.d("OMG", e.toString());
                    }
                }
        }
    }

    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context c) {
        super.onAttach(c);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
